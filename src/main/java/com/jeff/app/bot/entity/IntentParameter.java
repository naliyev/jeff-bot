package com.jeff.app.bot.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "intent_parameter", schema = "jeff", catalog = "postgres")
public class IntentParameter {
    @Id
    @Column(name = "param_id")
    private int paramId;
    @Column(name = "intent_id")
    private int intentId;
    @Column(name = "param_name")
    private String paramName;
}
