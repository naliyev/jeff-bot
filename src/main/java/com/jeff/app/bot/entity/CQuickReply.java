package com.jeff.app.bot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "c_quick_reply", schema = "jeff", catalog = "postgres")
public class CQuickReply extends Component {
    @Id
    @Column(name = "cid")
    private int cid;
    @Column(name = "component_id")
    private int componentId;
    @Column(name = "label")
    private String label;
    @Column(name = "action")
    private String action;
}
