package com.jeff.app.bot.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "conversation_param", schema = "jeff", catalog = "postgres")
public class ConversationParam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "conv_param_id")
    private int convParamId;
    @Column(name = "session_id")
    private int sessionId;
    @Column(name = "param_name")
    private String paramName;
    @Column(name = "param_value")
    private String paramValue;
}
