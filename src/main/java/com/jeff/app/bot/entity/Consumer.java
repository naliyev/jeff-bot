package com.jeff.app.bot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "consumer", schema = "jeff", catalog = "postgres")
public class Consumer {
    @Id
    @Column(name = "developer_id")
    private int developerId;
    @Column(name = "is_active")
    private boolean isActive;
}