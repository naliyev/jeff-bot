package com.jeff.app.bot.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "message_node", schema = "jeff", catalog = "postgres")
public class MessageNode {
    @Id
    @Column(name = "node_id")
    private int nodeId;
    @Column(name = "parent_node_id")
    private int parentNodeId;
    @Column(name = "story_id")
    private int storyId;
    @Column(name = "component_id")
    private Integer componentId;
    @Column(name = "intent_id")
    private int intentId;
}
