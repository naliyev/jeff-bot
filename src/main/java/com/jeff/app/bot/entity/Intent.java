package com.jeff.app.bot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name = "intent", schema = "jeff", catalog = "postgres")
public class Intent {
    @Id
    @Column(name = "intent_id")
    private int intentId;
    @Column(name = "intent_name")
    private String intentName;
}