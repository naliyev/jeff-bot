package com.jeff.app.bot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "story", schema = "jeff", catalog = "postgres")
public class Story {
    @Id
    @Column(name = "story_id")
    private int storyId;
    @Column(name = "developer_id")
    private int developerId;
}
