package com.jeff.app.bot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "component", schema = "jeff", catalog = "postgres")
public class Component {
    @Id
    @Column(name = "component_id")
    private int componentId;
    @Column(name = "text")
    private String text;
    @Column(name = "type")
    private char type;
}
