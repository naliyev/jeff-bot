package com.jeff.app.bot.service;

import com.jeff.app.bot.dao.ChatDAOImpl;
import com.jeff.app.bot.entity.ConversationParam;
import com.jeff.app.bot.entity.IntentParameter;
import com.jeff.app.bot.entity.MessageNode;
import com.jeff.app.bot.model.QueryRq;
import com.jeff.app.bot.model.Response;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ConversationEngineServiceImpl {

    @Autowired
    private ChatDAOImpl chatDAOImpl;

    // Intent Classification; Entity Extraction; Action; Responding
    public void intentClassification(QueryRq query) {
        List<MessageNode> messageNodeList = chatDAOImpl.getMessageNodes(query);

        if (messageNodeList.size() > 0) {
            List<IntentParameter> intentParameterList = chatDAOImpl.getIntentParams(messageNodeList.get(0));
            entityExtraction(intentParameterList, query);
        }
    }

    public void entityExtraction(List<IntentParameter> intentParameterList, QueryRq query) {
        if (intentParameterList.size() > 0) {
            chatDAOImpl.save(intentParameterList.get(0), query);
        }
    }

    public void action() {

    }

    public void responding(List<Response> responseList, QueryRq query) {
        if (responseList.size() == 0) return;

        List<String> matchList = new ArrayList<String>();
        Pattern regex = Pattern.compile("\\{(.*?)\\}");

        for (Response rs: responseList) {
            Matcher regexMatcher = regex.matcher(rs.getText());

            while (regexMatcher.find()) {
                matchList.add(regexMatcher.group(1));
            }
        }

        if (matchList.size() == 0) return;

        List<ConversationParam> resultList = chatDAOImpl.getConversationParams(matchList, query);

        Map<String, String> valuesMap = new HashMap<>();
        for (ConversationParam p: resultList) {
            valuesMap.put(p.getParamName(), p.getParamValue());
        }

        for (int i = 0; i < responseList.size(); i++) {
            String templateString = responseList.get(i).getText();
            StringSubstitutor sub = new StringSubstitutor(valuesMap);
            String resolvedString = sub.replace(templateString);

            responseList.get(i).setText(resolvedString);
        }


    }
}
