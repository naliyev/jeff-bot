package com.jeff.app.bot.service;

import com.jeff.app.bot.dao.ChatDAOImpl;
import com.jeff.app.bot.model.QueryRq;
import com.jeff.app.bot.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatServiceImpl {

    @Autowired
    private ChatDAOImpl chatDAOImpl;

    public int getChatSessionId(int developerId) {
        return chatDAOImpl.getChatSessionId(developerId);
    }

    public int test(int k, String v) {
        return chatDAOImpl.test(k, v);
    }

    public List<Response> query(QueryRq queryRq) {
        return chatDAOImpl.query(queryRq);
    }

}
