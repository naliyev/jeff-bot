package com.jeff.app.bot;


import com.jeff.app.bot.model.QueryRq;
import com.jeff.app.bot.model.Response;
import com.jeff.app.bot.service.ChatServiceImpl;
import com.jeff.app.bot.service.ConversationEngineServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class ChatController {

    @Autowired
    private ChatServiceImpl chatServiceImpl;

    @Autowired
    private ConversationEngineServiceImpl conversationEngineServiceImpl;

    @GetMapping("/start/{developerId}")
    public int start(@PathVariable int developerId) {
        return chatServiceImpl.getChatSessionId(developerId);
    }

    @PostMapping("/chat")
    public List<Response> query(@RequestBody QueryRq queryRq) {
        conversationEngineServiceImpl.intentClassification(queryRq);
        List<Response> responseList = chatServiceImpl.query(queryRq);
        conversationEngineServiceImpl.responding(responseList, queryRq);
        return responseList;
    }
}
