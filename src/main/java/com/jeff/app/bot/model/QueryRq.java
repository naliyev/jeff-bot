package com.jeff.app.bot.model;

import lombok.Data;

@Data
public class QueryRq {
    private Integer sessionId;
    private Integer storyId;
    private Integer replyNodeId;
    private String queryText;
}
