package com.jeff.app.bot.model;

import lombok.Data;

@Data
public class Response {
    private int nodeId;
    private int parentNodeId;
    private int storyId;
    private int componentId;
    private int intentId;
    private String text;
    private String type;
}
