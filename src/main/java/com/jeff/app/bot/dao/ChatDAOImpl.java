package com.jeff.app.bot.dao;

import com.jeff.app.bot.entity.*;
import com.jeff.app.bot.model.QueryRq;
import com.jeff.app.bot.model.Response;
import com.jeff.app.config.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ChatDAOImpl {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("namedParameterJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public int getChatSessionId(int developerId) {
        Session session = null;
        Transaction transaction = null;
        ChatSession chatSession = new ChatSession();

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            chatSession.setDeveloperId(developerId);
            session.save(chatSession);
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return chatSession.getSessionId();
    }

    public List<Response> query(QueryRq queryRq) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("storyId", queryRq.getStoryId());
        parameters.addValue("parentNodeId", queryRq.getReplyNodeId());

        String firstPartSql = null;

        if (queryRq.getReplyNodeId() == null) {
            firstPartSql = "SELECT * FROM jeff.message_node WHERE story_id = :storyId and parent_node_id IS NULL ";
        } else {
            firstPartSql = "SELECT * FROM jeff.message_node WHERE story_id = :storyId and parent_node_id = :parentNodeId ";
        }

        String sql = "WITH RECURSIVE a AS ( " +
                firstPartSql +
                " UNION ALL " +
                "SELECT d.* FROM jeff.message_node d JOIN a ON a.node_id = d.parent_node_id WHERE d.story_id = :storyId AND d.intent_id is NULL " +
                ") " +
                "SELECT a.node_id, a.parent_node_id, a.story_id, a.component_id, a.intent_id, c.text, c.type " +
                "  FROM a JOIN jeff.component c ON a.component_id = c.component_id";




        List<Response> listContact = namedParameterJdbcTemplate.query(sql, parameters, new RowMapper<Response>() {

            @Override
            public Response mapRow(ResultSet rs, int rowNum) throws SQLException {
                Response response = new Response();

                response.setNodeId(rs.getInt("node_id"));
                response.setParentNodeId(rs.getInt("parent_node_id"));
                response.setStoryId(rs.getInt("story_id"));
                response.setComponentId(rs.getInt("component_id"));
                response.setIntentId(rs.getInt("intent_id"));
                response.setText(rs.getString("text"));
                response.setType(rs.getString("type"));


                return response;
            }

        });

        return listContact;
    }

    public List<MessageNode> getMessageNodes(QueryRq queryRq) {

        Session session = null;
        Transaction transaction = null;
        ChatSession chatSession = new ChatSession();
        List<MessageNode> resultList = new ArrayList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<MessageNode> query = builder.createQuery(MessageNode.class);
            Root<MessageNode> root = query.from(MessageNode.class);
            query.select(root);

            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("parentNodeId"), queryRq.getReplyNodeId()));
            predicates.add(builder.equal(root.get("storyId"), queryRq.getStoryId()));

            query.where(predicates.toArray(new Predicate[0]));

            Query<MessageNode> q = session.createQuery(query);

            resultList = q.getResultList();

            transaction.commit();

        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return resultList;
    }

    public List<Intent> getIntents(MessageNode messageNode) {

        Session session = null;
        Transaction transaction = null;
        ChatSession chatSession = new ChatSession();
        List<Intent> resultList = new ArrayList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Intent> query = builder.createQuery(Intent.class);
            Root<Intent> root = query.from(Intent.class);
            query.select(root);

            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("parentMinerId"), messageNode.getIntentId()));
            query.where(predicates.toArray(new Predicate[0]));

            Query<Intent> q = session.createQuery(query);

            resultList = q.getResultList();

            transaction.commit();

        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return resultList;
    }

    public List<IntentParameter> getIntentParams(MessageNode messageNode) {

        Session session = null;
        Transaction transaction = null;
        ChatSession chatSession = new ChatSession();
        List<IntentParameter> resultList = new ArrayList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<IntentParameter> query = builder.createQuery(IntentParameter.class);
            Root<IntentParameter> root = query.from(IntentParameter.class);
            query.select(root);

            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("intentId"), messageNode.getIntentId()));
            query.where(predicates.toArray(new Predicate[0]));

            Query<IntentParameter> q = session.createQuery(query);

            resultList = q.getResultList();

            transaction.commit();

        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return resultList;
    }

    public List<ConversationParam> getConversationParams(List<String> matchList, QueryRq queryRq) {

        Session session = null;
        Transaction transaction = null;
        ChatSession chatSession = new ChatSession();
        List<ConversationParam> resultList = new ArrayList<>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<ConversationParam> query = builder.createQuery(ConversationParam.class);
            Root<ConversationParam> root = query.from(ConversationParam.class);
            query.select(root);

            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("sessionId"), queryRq.getSessionId()));
            predicates.add(root.get("paramName").in(matchList));
            query.where(predicates.toArray(new Predicate[0]));

            Query<ConversationParam> q = session.createQuery(query);

            resultList = q.getResultList();

            transaction.commit();

        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return resultList;
    }

    public int save(IntentParameter intentParameter, QueryRq query) {
        Session session = null;
        Transaction transaction = null;
        ConversationParam conversationParam = new ConversationParam();

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            conversationParam.setSessionId(query.getSessionId());
            conversationParam.setParamName(intentParameter.getParamName());
            conversationParam.setParamValue(query.getQueryText());

            session.save(conversationParam);
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e);
            if (transaction != null) {
                transaction.rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return conversationParam.getConvParamId();
    }




}

