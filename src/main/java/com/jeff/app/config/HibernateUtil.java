package com.jeff.app.config;

import com.jeff.app.bot.entity.ChatSession;
import com.jeff.app.bot.entity.ConversationParam;
import com.jeff.app.bot.entity.IntentParameter;
import com.jeff.app.bot.entity.MessageNode;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            Configuration configuration = new Configuration();
            configuration.configure("hibernate.cfg.xml");
            configuration.addAnnotatedClass(Person.class);
            configuration.addAnnotatedClass(ChatSession.class);
            configuration.addAnnotatedClass(MessageNode.class);
            configuration.addAnnotatedClass(IntentParameter.class);
            configuration.addAnnotatedClass(ConversationParam.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            return sessionFactory;
        }
        catch (HibernateException ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }
}
