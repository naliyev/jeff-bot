package com.jeff.app.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public HikariDataSource dataSourceJDBC() {
        HikariConfig config = new HikariConfig();
        //env.getProperty("mongodb.url")

        config.setDriverClassName(env.getProperty("spring.datasource.postgres.driver-class-name"));
        config.setJdbcUrl(env.getProperty("spring.datasource.postgres.jdbc-url"));
        config.setUsername(env.getProperty("spring.datasource.postgres.username"));
        config.setPassword(env.getProperty("spring.datasource.postgres.password"));
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        return new HikariDataSource(config);
    }

    @Bean
    public JdbcTemplate jdbcTemplateSVBO(@Qualifier("dataSourceJDBC") DataSource ds) {
        return new JdbcTemplate(ds);
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("dataSourceJDBC") DataSource ds) {
        return new NamedParameterJdbcTemplate(ds);
    }
}
